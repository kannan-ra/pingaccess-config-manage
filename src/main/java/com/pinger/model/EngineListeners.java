package com.pinger.model;

import com.pingidentity.pa.adminui.command.engine.listener.EngineListenerView;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@Data
@ConfigurationProperties(prefix="networking.engineListener")
public class EngineListeners {

    List<Listener> listenerList;

    @Data
    public static class Listener {
        String name;
        int port;
        boolean secure;

        // compare primary
        public boolean compareKey(EngineListenerView serverItem) {
            if (serverItem.getPort() == port)
                return true;
            else
                return false;
        }

        // compare with server item
        public boolean compareValue(EngineListenerView serverItem) {
            if (name.equals(serverItem.getName())
                    && secure == serverItem.isSecure())
                return true;
            else
                return false;
        }

        // build server item from config
        public static EngineListenerView buildServerItem(Listener listener) {
            EngineListenerView serverItem = new EngineListenerView();
            serverItem.setName(listener.getName());
            serverItem.setPort(listener.getPort());
            serverItem.setSecure(listener.isSecure());
            return serverItem;
        }
    }
}
