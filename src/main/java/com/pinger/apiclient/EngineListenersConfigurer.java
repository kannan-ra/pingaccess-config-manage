package com.pinger.apiclient;

import com.pinger.application.RestTemplateConfig;
import com.pinger.model.EngineListeners;
import com.pingidentity.pa.adminui.command.engine.listener.EngineListenerView;
import com.pingidentity.pa.adminui.view.EngineListenersView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Service
@ConfigurationProperties(prefix="host")
@EnableConfigurationProperties({RestTemplateConfig.class, EngineListeners.class})
public class EngineListenersConfigurer {

    private static final Logger log = LogManager.getLogger(EngineListenersConfigurer.class.getName());

    private static String API_RESOURCE = "engineListeners/";

    @Getter @Setter
    String apiBaseUrl;

    @Autowired
    EngineListeners configEngineListeners;

    @Autowired
    RestTemplate restTemplate;

    public void configure() {

        // obtain the list from the Server with a GET
        EngineListenersView serverEngineListeners = restTemplate.getForObject(
                apiBaseUrl + API_RESOURCE,
                EngineListenersView.class);

        // Server Items that are handled - a tracker
        List<EngineListenerView> trackedServerItemList = new ArrayList<>();

        // Compare the Config Objects with Server Objects
        if (configEngineListeners.getListenerList() != null) {
            for (EngineListeners.Listener configItem : configEngineListeners.getListenerList()) {
                boolean configItemFound = false;
                for (EngineListenerView serverItem : serverEngineListeners.getItems()) {
                    if (configItem.compareKey(serverItem)) {
                        if (!configItem.compareValue(serverItem)) {
                            // call the PUT to update
                            restTemplate.put(apiBaseUrl + API_RESOURCE + serverItem.getId(),
                                    EngineListeners.Listener.buildServerItem(configItem));
                            log.info("Updated - " + configItem.toString());
                        }
                        trackedServerItemList.add(serverItem);
                        configItemFound = true;
                        break;
                    }
                }
                if (!configItemFound) {
                    // call POST my config
                    ResponseEntity<EngineListenerView> responseEntity = restTemplate.postForEntity(
                            apiBaseUrl + API_RESOURCE,
                            EngineListeners.Listener.buildServerItem(configItem),
                            EngineListenerView.class);
                    log.info(responseEntity.getStatusCode() + " Created - " + configItem.toString());
                }
            }
        }

        // remove the ones that are handled
        serverEngineListeners.getItems().removeAll(trackedServerItemList);

        // loop through remaining items & trigger DELETE
        for (EngineListenerView serverItem : serverEngineListeners.getItems()) {
            restTemplate.delete(apiBaseUrl + API_RESOURCE + serverItem.getId());
            log.info("Deleted - Item with Id:" + serverItem.getId());
        }
    }
}
