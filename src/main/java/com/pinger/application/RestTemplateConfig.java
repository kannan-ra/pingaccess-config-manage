package com.pinger.application;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@ConfigurationProperties(prefix="admin")
@Data
public class RestTemplateConfig {

    private String username;
    private String password;

    @Bean
    public RestTemplate restTemplate() {
        SSLCertificateValidation.disable();
        return new RestTemplateBuilder()
                .basicAuthorization(username, password)
                .additionalInterceptors(new CustomHeaderInterceptor())
                .build();
    }
}