## Configuring Ping Access


#### Configurations

`@ConfigurationProperties` annotation is used to autowire the spring beans from properties file.
Open the `application-dev.properties` file and set your own configurations. Use another file for different environment.

#### Prerequisites

- Java 8
- Maven > 3.0

#### Build and run

Go on the project's root folder, then type:
```
mvn spring-boot:run -Drun.profiles=dev
```